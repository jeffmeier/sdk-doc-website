module.exports = {
  someSidebar: {
    'Getting Started': ['GettingStarted', 'QuickStart'],
    'Work with Data': ['FindingData', 'CreateObjects', 'SettingObjectMetadata'],
    'Data Model':['hierarchy', 'permissions', 'custom_roles', 'containers'],
    'Flywheel Views':['views'],
    'SDK Gears':['sdk_gears'],
    'Tutorials':['tutorial_Redcap']
  },
};
